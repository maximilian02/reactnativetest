import { createStackNavigator } from 'react-navigation';
import HomeScreen from './src/screens/home/Main';
import ResultsScreen from './src/screens/results/Main';
import PlaceScreen from './src/screens/place/Main';

const App = createStackNavigator(
  {
    Home: HomeScreen,
    Results: ResultsScreen,
    Place: PlaceScreen
  },
  {
    initialRouteName: 'Home',
  }
);

export default App;
