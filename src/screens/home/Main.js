import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   padding: 10
  },
  title: {
    fontSize: 30,
    textAlign: 'center'
  },
  subtitle: {
    fontSize: 55,
    textAlign: 'center'
  },
  input: { 
    height: 40
  }
});

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { query: '' };
  }

  _onChangeHandler = (query) => this.setState({ query });

  _executeSearch = () => {
    const { navigate } = this.props.navigation;
    return navigate('Results', { query: this.state.query });
  };
  
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Bienvenido a guia</Text>
        <Text style={styles.subtitle}>Dominicana</Text>
        <TextInput
          style={styles.input}
          placeholder="Clubs, Restaurantes, Excursiones..."
          onChangeText={this._onChangeHandler}
        />
        <Button
          title="Buscar!"
          onPress={this._executeSearch}
        />
      </View>
    );
  }
}

export default HomeScreen;