import React, { Component } from 'react';
import { SectionList, StyleSheet, Text, View } from 'react-native';
import getPlaces from 'DominicanaApp/src/shared/actions/getPlaces';

const styles = StyleSheet.create({
  container: {
   flex: 1,
   padding: 10
  },
  list: {
    marginTop: 20
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  }
});

class ResultsScreen extends Component {
  constructor(props) {
    super(props);
    const { navigation } = props;
    this.state = {
      query: navigation.getParam('query', 'NO-QUERY')
    };
    getPlaces().then(({ data }) => {
      console.log('res', data);
    });
  }

  _renderItem = ({ item }) => (<Text style={styles.item}>{item}</Text>);
  _renderSectionHeader = ({ section }) => (<Text style={styles.sectionHeader}>{section.title}</Text>);

  render() {
    const { query } = this.state;
    return (
      <View style={styles.container}>
        <Text>Resultados de búsqueda: "{query}"</Text>
        <SectionList
          style={styles.list}
          sections={[
            {title: 'D', data: ['Devin']},
            {title: 'J', data: ['Jackson', 'James', 'Jillian', 'Jimmy', 'Joel', 'John', 'Julie']}
          ]}
          renderItem={this._renderItem}
          renderSectionHeader={this._renderSectionHeader}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
};

export default ResultsScreen;