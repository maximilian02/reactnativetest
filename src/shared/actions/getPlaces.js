const axios = require('axios');

const apiBase = 'http://auctioneerland.com:3000/api/';

const getPlaces = () => {
  return axios.get(apiBase + 'places')
    .then(res => res.data)
    .catch(function (error) {
      console.log(error);
    });
}

export default getPlaces;